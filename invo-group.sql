-- phpMyAdmin SQL Dump
-- version 4.0.10deb1
-- http://www.phpmyadmin.net
--
-- Хост: localhost
-- Время создания: Мар 09 2016 г., 11:59
-- Версия сервера: 5.5.46-0ubuntu0.14.04.2
-- Версия PHP: 5.5.9-1ubuntu4.14

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- База данных: `invo-group`
--

-- --------------------------------------------------------

--
-- Структура таблицы `ig_test_contact_form`
--

CREATE TABLE IF NOT EXISTS `ig_test_contact_form` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `phone` varchar(15) NOT NULL,
  `email` varchar(50) NOT NULL,
  `comment` text NOT NULL,
  `address` varchar(255) NOT NULL,
  `site` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=68 ;

--
-- Дамп данных таблицы `ig_test_contact_form`
--

INSERT INTO `ig_test_contact_form` (`id`, `phone`, `email`, `comment`, `address`, `site`) VALUES
(63, '0000000', 'qqqqqq@wwwww.ee', 'qwqsqdcdscsdf', '', ''),
(64, '77771111', 'qqqqqq@wwwww.ee', 'mmmmdsmmmdmmd', '', ''),
(65, '534848', 'dwvew@sdfv.df', 're3d3d2f2 f2e3fcm,', '', ''),
(66, '34563532', 'vgy@erfgh.tyuy', '5652252xdmkdkd', '', ''),
(67, '+380661234567', 'wergerwg@sfsdg.df', 'kkklljl', '', '');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
