<?php

/**
 * This is the model class for table "ig_test_contact_form".
 *
 * The followings are the available columns in table 'ig_test_contact_form':
 * @property string $id
 * @property string $phone
 * @property string $email
 * @property string $comment
 * @property string $address
 * @property string $site
 */
class TestContactForm extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'ig_test_contact_form';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('phone, email, comment', 'required'),
			array('phone', 'length', 'max'=>15),
			array('email, site', 'length', 'max'=>50),
			array('address', 'length', 'max'=>255),
			array('email', 'email'),
			array('site', 'url'),
			array('phone', 'match', 'pattern'=>'/^([+]?[0-9 ]+)$/', 'message' => 'Некорректный номер телефона' ),
//			array('check-spam', 'match', 'pattern'=>'', 'message' => 'Oooops...' ),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, phone, email, comment, address, site', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'phone' => 'Телефон',
			'email' => 'Email',
			'comment' => 'Комментарий',
			'address' => 'Адрес',
			'site' => 'Сайт',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('phone',$this->phone,true);
		$criteria->compare('email',$this->email,true);
		$criteria->compare('comment',$this->comment,true);
		$criteria->compare('address',$this->address,true);
		$criteria->compare('site',$this->site,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return TestContactForm the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	protected function afterSave()
	{
		if (parent::afterSave()) {
			// Send email
		}
	}

	public function validate()
	{
		if (isset($_POST['check-spam']) && $_POST['check-spam'] != '') {
			$this->addError('check-spam', 'Oooops');

			return false;
		}

		return parent::validate();
	}
}
