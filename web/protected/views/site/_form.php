<?php if(Yii::app()->user->hasFlash('testform')) { ?>
    <div class="flash-success">
        <?php echo Yii::app()->user->getFlash('testform'); ?>
    </div>
<?php } ?>
<?php if (! $is_sent) { ?>
<div class="form">
    <?php $form = $this->beginWidget('CActiveForm', array(
        'id'	=> 'test-form',
        'enableAjaxValidation' => true,
        'clientOptions' => array(
            'validateOnSubmit' => true,
            'validateOnChange' => false,
        ),
    )); ?>
    <p class="note">Поля со знаком <span class="required">*</span> являются обязательными</p>
    <?php echo $form->errorSummary($model); ?>

    <?php echo CHtml::textField('check-spam', '', array('class' => 'hide')); ?>
    <div class="row">
        <?php echo $form->labelEx($model,'phone'); ?>
        <?php echo $form->textField($model,'phone'); ?>
        <?php echo $form->error($model,'phone'); ?>
    </div>
    <div class="row">
        <?php echo $form->labelEx($model,'email'); ?>
        <?php echo $form->textField($model,'email'); ?>
        <?php echo $form->error($model,'email'); ?>
    </div>
    <div class="row">
        <?php echo $form->labelEx($model,'comment'); ?>
        <?php echo $form->textArea($model,'comment',array('rows'=>5, 'cols'=>40)); ?>
        <?php echo $form->error($model,'comment'); ?>
    </div>
    <div class="row submit">
        <?php echo CHtml::ajaxSubmitButton('Обработать', CHtml::normalizeUrl(array('site/TestFormAjax')), array(
            'type' => 'POST',
            // Результат запроса записываем в элемент, найденный по CSS-селектору #output.
            'update' => '.form',
//            'dataType'=>'json',
        ),
            array(
                'type' => 'submit'
            ));  ?>
    </div>
    <?php $this->endWidget(); ?>
</div>
<?php }