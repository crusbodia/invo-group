<?php
/* @var $this SiteController */

$this->pageTitle = Yii::app()->name . ' - Форма обратной связи';
$this->breadcrumbs=array(
	'Форма обратной связи',
);
?>

<h1>Форма обратной связи</h1>


<?php echo $this->renderPartial('_form', array('model'=>$model, 'is_sent' => $is_sent)); ?>

